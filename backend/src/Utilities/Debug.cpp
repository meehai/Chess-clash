#include <Utilities/Debug.hpp>

constexpr int DebugFlag::FlagTrue;
constexpr int DebugFlag::FlagFalse;
constexpr int DebugFlag::PrintSpawning;
constexpr int DebugFlag::PrintIniSettingsFile;
constexpr int DebugFlag::PrintGameSettings;
constexpr int DebugFlag::PrintChildMessages;
constexpr int DebugFlag::PrintReadyChessProcess;
constexpr int DebugFlag::PrintGameEachTurn;

/* The false counterpart is defined in .hpp becuase it's templated */
Debug<true> Debug<true>::writer;

