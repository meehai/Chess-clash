#ifndef PAIR_H
#define PAIR_H

/* TODO: Add iterator/iteration method for easy access/iteration in fors */
template <typename T, typename U>
struct Pair {
    T x;
    U y;
    constexpr Pair() : x(0), y(0) { }
    constexpr Pair(T xx, U yy) : x(xx), y(yy) { }
    constexpr int first() const { return x; }
    constexpr int second() const { return y; }
};

#endif /* PAIR_H */