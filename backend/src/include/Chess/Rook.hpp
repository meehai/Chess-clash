/* Rook.h - Header for a rook piece */
#ifndef ROOK_H
#define ROOK_H

#include <Chess/Piece.hpp>

struct RookTable {
	static constexpr int index(int i, int j, int k) {
		return i * 8 * 15 + j * 15 + k;
	}

	constexpr RookTable() : values()
	{
		constexpr int directionsI[4] = {-1, +1, 0, 0};
		constexpr int directionsJ[4] = {0, 0, -1, +1};
		for (auto i=0; i<8; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;
				for(auto k=0; k<4; ++k) {
					for(auto inner_i=i+directionsI[k], inner_j=j+directionsJ[k]; inner_i>=0 && inner_i<=7 && 
						inner_j>=0 && inner_j<=7; inner_i+=directionsI[k], inner_j+=directionsJ[k]) {
						values[index(i, j, p)] = Pair<int, int>(inner_i, inner_j);
						++p;
					}
				}
				values[index(i, j, p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	struct Pair<int, int> values[8*8*15];
};

class Rook : public Piece {
public:
	Rook(Color);
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int, 
		std::tuple<int, int, int, int>);
	~Rook();

	constexpr static RookTable rookTable = RookTable();
	bool canCastle;
	bool lastMoveCouldCastle;
};

#endif // ROOK_H