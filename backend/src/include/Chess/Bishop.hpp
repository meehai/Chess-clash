/* Bishop.h - Header for a bishop piece */
#ifndef BISHOP_H
#define BISHOP_H

#include <Chess/Piece.hpp>

struct BishopTable {
	static constexpr int index(int i, int j, int k) {
		return i * 8 * 15 + j * 15 + k;
	}

	constexpr BishopTable() : values()
	{
		constexpr int directions[2] = {-1, +1};
		for (auto i=0; i<8; ++i) {
			for(auto j=0; j<8; ++j) {
				int p = 0;
				for(auto direction_i : directions) {
					for(auto direction_j : directions) {
						for(auto inner_i=i+direction_i, inner_j=j+direction_j; inner_i>=0 && inner_i<=7 &&
							inner_j>=0 && inner_j<=7; inner_i+=direction_i, inner_j+=direction_j) {
							values[index(i, j, p)] = Pair<int, int>(inner_i, inner_j);
							++p;
						}
					}
				}
				values[index(i, j, p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	struct Pair<int, int> values[8*8*15];
};

class Bishop : public Piece {
public:
	Bishop(Color);
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int, 
		std::tuple<int, int, int, int>);
	~Bishop();

	constexpr static BishopTable bishopTable = BishopTable();
};

#endif // BISHOP_H