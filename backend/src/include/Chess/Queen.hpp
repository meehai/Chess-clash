/* Queen.h - Header for a queen piece */
#ifndef QUEEN_H
#define QUEEN_H

#include <Chess/Piece.hpp>
#include <Chess/Bishop.hpp>
#include <Chess/Rook.hpp>

class Queen : public Piece {
	friend class Rook;
	friend class Bishop;
public:
	Queen(Color);
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int, 
		std::tuple<int, int, int, int>);
	~Queen();
};

#endif // QUEEN_H