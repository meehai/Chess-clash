/* Pawn.h - Header for a pawn piece */
#ifndef PAWN_H
#define PAWN_H

#include <Chess/Piece.hpp>

/* Tables for positions to verify if a king is in check by a pawn. */
struct PawnCheckTable {
	static constexpr int index(int i, int j, int p) {
		return i * 8 * 3 + j * 3 + p;
	}
};
/* If the king is black, we look for white pawns that can threaten him. (racism, i know) */
struct BlackPawnCheckTable : PawnCheckTable {
	constexpr BlackPawnCheckTable() : values()
	{
		for (auto i=0; i<8; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;
				/* Note: pawns can't be on first or last rank */
				/* Black values */
				if(i-1>=1 && j-1>=0) {
					values[index(i, j, p)] = Pair<int, int>(i-1, j-1);
					++p;
				}
				if(i-1>=1 && j+1<=7) {
					values[index(i, j, p)] = Pair<int, int>(i-1, j+1);
					++p;
				}
				values[index(i, j, p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	struct Pair<int, int> values[8*8*3];
};

struct WhitePawnCheckTable : PawnCheckTable {
	constexpr WhitePawnCheckTable() : values()
	{
		for (auto i=0; i<8; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;
				/* Note: pawns can't be on first or last rank */
				/* White values */
				if(i+1<=6 && j-1>=0) {
					values[index(i, j, p)] = Pair<int, int>(i+1, j-1);
					++p;
				}
				if(i+1<=6 && j+1<=7) {
					values[index(i, j, p)] = Pair<int, int>(i+1, j+1);
					++p;
				}
				values[index(i, j, p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	struct Pair<int, int> values[8*8*3];
};

struct PawnMoveTable {
	static constexpr int index(int i, int j, int p) {
		return i * 8 * 5 + j * 5 + p;
	}
};

/* Tables for positions where a pawn can move in the next move. */
/* White pawns for white to move */
struct WhitePawnMoveTable : PawnMoveTable {
	constexpr WhitePawnMoveTable() : values()
	{
		for (auto i=1; i<7; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;
				if(j-1 >= 0) {
					values[index(i, j, p)] = Pair<int, int>(i+1, j-1);
					++p;
				}
				
				values[index(i, j, p)] = Pair<int, int>(i+1, j);
				++p;

				if(j+1 <= 7) {
					values[index(i, j, p)] = Pair<int, int>(i+1, j+1);
					++p;
				}

				if(i == 1) {
					values[index(i, j, p)] = Pair<int, int>(i+2, j);
					++p;
				}

				values[index(i, j, p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	struct Pair<int, int> values[8*8*5];
};

struct BlackPawnMoveTable : PawnMoveTable {
	constexpr BlackPawnMoveTable() : values()
	{
		for (auto i=1; i<7; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;

				if(j - 1 >= 0) {
					values[index(i, j, p)] = Pair<int, int>(i-1, j-1);
					++p;
				}

				values[index(i, j, p)] = Pair<int, int>(i-1, j);
				++p;

				if(j + 1 <= 7) {
					values[index(i, j, p)] = Pair<int, int>(i-1, j+1);
					++p;
				}

				if(i == 6) {
					values[index(i, j, p)] = Pair<int, int>(i-2, j);
					++p;
				}

				values[index(i, j, p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	struct Pair<int, int> values[8*8*5];
};

class Pawn : public Piece {
public:
	Pawn(Color);
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int, 
		std::tuple<int, int, int, int>);
	~Pawn();

	constexpr static WhitePawnCheckTable whitePawnCheckTable = WhitePawnCheckTable();
	constexpr static BlackPawnCheckTable blackPawnCheckTable = BlackPawnCheckTable();
	constexpr static WhitePawnMoveTable whitePawnMoveTable = WhitePawnMoveTable();
	constexpr static BlackPawnMoveTable blackPawnMoveTable = BlackPawnMoveTable();
};

#endif // PAWN_H