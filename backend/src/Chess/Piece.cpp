#include <Chess/Piece.hpp>

char Piece::toChar() {
	return this->symbol;
}

std::string &Piece::getType() {
	return this->type;
}

bool Piece::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> position) {
	return false;
}

Color Piece::getColor() {
	return this->color;
}
