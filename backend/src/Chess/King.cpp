#include <Chess/King.hpp>

constexpr KingTable King::kingTable;

King::King(Color color) : Piece(color) { 
	this->symbol = (color == Color::White) ? 'K' : 'k';
	this->type = "King";
	this->canCastle = true;
}

bool King::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> positions) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;

	/* King can also castle so we can jump 2 squares. Aditional check will be done after */
	if(abs(oldI-newI) > 1 || abs(oldJ-newJ) > 2)
		return false;

	return true;
}

King::~King() { }
