#include <Chess/Rook.hpp>

constexpr RookTable Rook::rookTable;

Rook::Rook(Color color) : Piece(color) {
	this->symbol = (color==Color::White) ? 'R' : 'r';
	this->type = "Rook";
	this->canCastle = true;
}

bool Rook::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> positions) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;

	if(oldI != newI && oldJ != newJ)
		return false;

	int i_inc = (oldI==newI) ? 0 : 1 * (newI-oldI), j_inc = (oldJ==newJ) ? 0 : 1 * (newJ-oldJ);
	int i = oldI + i_inc, j = oldJ + j_inc;
	while(i != newI && j != newJ) {
		if(board[i][j]->getType() != "NoPiece")
			return false;
		i += i_inc;
		j += j_inc;
	}

	return true;
}

Rook::~Rook() { }
