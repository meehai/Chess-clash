#include <Chess/Pawn.hpp>

constexpr WhitePawnCheckTable Pawn::whitePawnCheckTable;
constexpr BlackPawnCheckTable Pawn::blackPawnCheckTable;
constexpr WhitePawnMoveTable Pawn::whitePawnMoveTable;
constexpr BlackPawnMoveTable Pawn::blackPawnMoveTable;

Pawn::Pawn(Color color) : Piece(color) {
	this->symbol = (color == Color::White) ? 'P' : 'p';
	this->type = "Pawn";
}

bool Pawn::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> positions) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;
	if(abs(oldJ-newJ) >= 2 || abs(oldI-newI) >= 3)
		return false;

	/* Cannot move forward over any piece */
	if(abs(oldJ-newJ) == 0 && board[newI][newJ]->getType() != "NoPiece")
		return false;

	/* Can move 2 positions only if on starting position and there's no piece between and also we move only forward */
	if(abs(oldI-newI) == 2 && (this->lastMoved != -1 || board[(oldI+newI)/2][oldJ]->getType() != "NoPiece" 
		|| abs(oldJ-newJ) != 0) )
		return false;

	/* Takes an opponent piece, so it must be a piece of opposite color or enpessant */
	if(abs(oldJ-newJ) == 1) {
		auto newPiece = board[newI][newJ];
		/* En pessant */
		if(newPiece->getType() == "NoPiece") {
			auto enPessantPiece = board[oldI][newJ];
			if(enPessantPiece->getType() != "Pawn"
				|| Color::getOppositeColor(enPessantPiece->getColor()) != this->getColor())
				return false;
			if(enPessantPiece->lastMoved != moveNumber - 1)
				return false;
		}
		/* Take opponent's piece */
		else {
			if(Color::getOppositeColor(newPiece->getColor()) != this->getColor())
				return false;
		}
	}

	return true;
}

Pawn::~Pawn() { }
