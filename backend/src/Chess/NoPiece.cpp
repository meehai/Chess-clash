#include <Chess/NoPiece.hpp>

std::shared_ptr<NoPiece> NoPiece::noPiece = std::make_shared<NoPiece>();

NoPiece::NoPiece() {
	this->color = Color::None;
	this->symbol = '_';
	this->type = "NoPiece";
}

std::shared_ptr<NoPiece> NoPiece::get() {
	return std::shared_ptr<NoPiece>(NoPiece::noPiece);
}


NoPiece::~NoPiece() { }
