#include <ChessGame.hpp>

std::map<std::string, int> ChessGame::CommunicationProtocol::settings = {
	{"Color", Color::White},
	{"Time", 10},
	{"Increment", 0},
	{"RepetitionDraw", 3}
};

std::map<std::string, int> ChessGame::piecesBitMapOrder = {
	{"Pawn", 0},
	{"Knight", 1},
	{"Bishop", 2},
	{"Rook", 3},
	{"Queen", 4},
	{"King", 5}
};

ChessGame::ChessGame(std::string configFile, std::string logFile) : logger(logFile) {
	this->board = std::vector<std::vector<std::shared_ptr<Piece>>>(ChessGame::boardSize, 
		std::vector<std::shared_ptr<Piece>>(ChessGame::boardSize));
	this->configFile = std::move(configFile);
	this->parseSettings();
	this->buildTable();
	this->modifiedPieces.reserve(4); /* Up to 4 pieces that can be changed during a move */
}

std::string ChessGame::toString() const {
	std::string str;

	for(int i=ChessGame::boardSize - 1; i>=0; i--) {
		str.append(std::to_string(i + 1)).append(" ");
		for(int j=0; j<ChessGame::boardSize; ++j) {
			str.append(1, this->board[i][j]->toChar()).append(" ");
		}
		str += "\n";
	}

	str.append("  ");
	for(int i=0; i<ChessGame::boardSize; ++i)
		str.append(1, 'a' + i).append(" ");
	str.append("\n");

	return str;
}

void ChessGame::buildTable() {
	for(int i=0; i<ChessGame::boardSize; ++i) {
		if(i == 0 || i == ChessGame::boardSize - 1) {
			Color color = (i == 0) ? Color::White : Color::Black;
			this->board[i][0] = std::make_shared<Rook>(color);
			this->board[i][1] = std::make_shared<Knight>(color);
			this->board[i][2] = std::make_shared<Bishop>(color);
			this->board[i][3] = std::make_shared<Queen>(color);
			this->board[i][4] = std::make_shared<King>(color);
			this->board[i][5] = std::make_shared<Bishop>(color);
			this->board[i][6] = std::make_shared<Knight>(color);
			this->board[i][7] = std::make_shared<Rook>(color);
		}
		else if(i == 1 || i == ChessGame::boardSize - 2) {
			Color color = (i == 1) ? Color::White : Color::Black;
			for(int j=0; j<ChessGame::boardSize; ++j)
				this->board[i][j] = std::make_shared<Pawn>(color);
		}
		else {
			auto noPiece = std::make_shared<NoPiece>();
			for(int j=0; j<ChessGame::boardSize; ++j)
				this->board[i][j] = NoPiece::get();
		}
	}
}

void ChessGame::parseProcessSettings(const IniReader &reader) {
	char **processArguments[numProcesses], *buf, *p;
	int processArgc[numProcesses];
	std::string processArgs, processPath;
	size_t pos;

	assert(reader.isSet("Processes", "Process1") && reader.isSet("Processes", "Process2") &&
		"Paths for child processes were not set.");
	for(int i=0; i<numProcesses; ++i) {
		processPath = reader.getValue("Processes", "Process" + std::to_string(i+1));
		processArgs = reader.getValue("Processes", "Args" + std::to_string(i+1));
		std::vector<size_t> positions{0};
		pos = -1;

		/* If the process has arguments, save the positions of the spaces, so strdup can work */
		while( (pos = processArgs.find(" ", pos + 1)) != std::string::npos)
			positions.push_back(pos);
		positions.push_back(processArgs.size());

		buf = strdup(processArgs.c_str());
		p = buf;
		processArgc[i] = positions.size();
		processArguments[i] = (char **)malloc( (positions.size() + 1) * sizeof(char *));
		processArguments[i][0] = strdup(processPath.c_str());
		for(unsigned int j = 1; j < positions.size(); ++j) {
			buf[positions[j]] = 0;
			processArguments[i][j] = strdup(p);
			p = &buf[positions[j]] + 1;
		}
		processArguments[i][positions.size()] = nullptr;

		free(buf);
		this->process[i].setArguments(processArgc[i], processArguments[i]);
	}
}

void ChessGame::parseSettings() {
	IniReader reader(this->configFile);
	std::string value;

	Debug<DebugFlag::PrintIniSettingsFile>::get()<<reader.toString()<<"\n";
	parseProcessSettings(reader);

	/* After having parsed the process settings, I must now parse the Chess game rules from the GameSettings section */
	for (auto &it: CommunicationProtocol::settings) {
		auto str = it.first;
		value = reader.getValue("GameSettings", str);
		int intValue;

		try {
			intValue = std::stoi(value);
		} catch (const std::exception& e) {
			std::cerr<<"Error converting '" << str << "' to integer\n";
			exit(1);
		}

		CommunicationProtocol::settings[str] = intValue;
	}
}

void ChessGame::startChessProcesses() {
	for(int i=0; i<numProcesses; ++i)
		this->process[i].startProcess();
}

void ChessGame::startChessGame() {
	std::string word;
	
	word.reserve(ChessGame::bufferSize);
	Debug<DebugFlag::PrintGameSettings>::get()<<"Game Settings: "<<CommunicationProtocol::toString()<<"\n";
	for(int i=0; i<numProcesses; ++i) {
		/* Read the init message from both processes, so we know they want to start a game. */
		assert(process[i].read(word, ChessGame::bufferSize, PipedProcess::ReadFlags::NoSpecialChars 
			| PipedProcess::ReadFlags::Timer) > 0);
		assert(word == CommunicationProtocol::initMessage);
		
		/* After reading the init message, send each of them the settings message for the game. */
		assert(process[i].writeln(CommunicationProtocol::toString(i == 1)) > 0);

		/* After both processes have the same settings, we must wait until they are ready to start a game */
		assert(process[i].read(word, ChessGame::bufferSize, PipedProcess::ReadFlags::NoSpecialChars
			| PipedProcess::ReadFlags::Timer) > 0);
		assert(word == CommunicationProtocol::readyMessage);
		Debug<DebugFlag::PrintReadyChessProcess>::get() << "Process " << i << " is ready" << "\n";
	}

	/* Now that the processes are ready, set game's variables for both players */
	int colorValue = Color::getColor(CommunicationProtocol::settings["Color"]).getValue();
	int white = colorValue; /* If colorValue is 0, player 0 is white, else player 1 is white */
	int black = white ^ 1;

	/* As per our protocol, only send the White player the "GO" message. Black will instead receive white's move. */
	assert(process[white].writeln(CommunicationProtocol::goMessage) > 0);

	/* Player data initialization */
	player[white].color = Color::White;
	player[white].kingPosition = {0, 4};
	player[white].opponent = std::unique_ptr<struct Player>(&player[black]);
	player[white].playerTimer = std::chrono::seconds(CommunicationProtocol::settings["Time"]);
	for(int i=0; i<2; i++) {
		for(int j=0; j<8; j++) {
			player[white].piecesBitMap[piecesBitMapOrder[board[i][j]->getType()]].set(i*8 + j);	
			player[white].piecesPosition.emplace(std::make_tuple(i, j));
		}
	}

	player[black].color = Color::Black;
	player[black].kingPosition = {7, 4};
	player[black].opponent = std::unique_ptr<struct Player>(&player[white]);
	player[black].playerTimer = std::chrono::seconds(CommunicationProtocol::settings["Time"]);
	for(int i=6; i<8; i++) {
		for(int j=0; j<8; j++) {
			player[black].piecesBitMap[piecesBitMapOrder[board[i][j]->getType()]].set(i*8 + j);
			player[black].piecesPosition.emplace(std::make_tuple(i, j));
		}
	}
}

std::string ChessGame::CommunicationProtocol::toString(bool flipTeams) {
	std::string str;
	int i = 0;

	for(auto &kv : CommunicationProtocol::settings) {
		/* flip team settings for the second process (white to black) */
		std::string second;
		if(flipTeams)
			second = std::to_string(kv.second ^ 1);
		else
			second = std::to_string(kv.second);

		if(i > 0)
			str.append("|");
		str.append(kv.first).append("=").append(second);
		++i;
	}

	return str;
}

void ChessGame::drawRequest(const std::string &_message, int turn) {
	std::string word;

	word.reserve(ChessGame::bufferSize);
	turn ^= 1;
	/* Write to the opponent the draw request and read the decision */
	assert(process[turn].writeln(_message) > 0);
	assert(process[turn].read(word, ChessGame::bufferSize, PipedProcess::ReadFlags::NoSpecialChars
		| PipedProcess::ReadFlags::Timer) > 0);
	Debug<DebugFlag::PrintGameEachTurn>::get() << "[process - " << turn << "] " << word << "\n";

	/* Write to the game logger. */
	if(moveNumber % 2 == 0)
		this->logger << word << " ";
	else
		this->logger << word << "\n";

	/* If opponent decides draw, the game is drawn. */
	if(word == CommunicationProtocol::drawMessage) {
		assert(process[turn].writeln(CommunicationProtocol::drawMessage) > 0);
		assert(process[turn^1].writeln(CommunicationProtocol::drawMessage) > 0);
		Debug<DebugFlag::EndGameStatus>::get() << "Draw!\n";
		this->logger << "\nDraw!\n";
		this->exitGame(0);
	}
	/* Else if the opponent decides to continue playing, the game is continued and the requester is sent GO */
	else if(word == CommunicationProtocol::goMessage) {
		assert(process[turn^1].writeln(word) > 0);
	}
	/* Anything else is invalid and the requester wins. */
	else {
		assert(process[turn].writeln(CommunicationProtocol::invalidMoveMessage) > 0);
		assert(process[turn^1].writeln(CommunicationProtocol::winMessage) > 0);
		Debug<DebugFlag::PrintGameEachTurn>::get() << "Invalid draw request response!\n";
		this->logger << "\nInvalid draw request response!\n";
		this->exitGame(0);
	}
}

void ChessGame::playGame() {
	int turn;
	std::string word;

	word.reserve(ChessGame::bufferSize);
	this->startChessGame();

	turn = player[0].color.getValue();
	Debug<DebugFlag::PrintGameEachTurn>::get() << "Starting Game. Process " << turn << " is White\n";
	Debug<DebugFlag::PrintGameEachTurn>::get() << this->toString() << "\n";
	this->logger << "Starting Game. Process " << turn << " is White\n";

	while(true) {

		if(moveNumber % 2 == 0)
			this->logger << moveNumber / 2 << ". ";

		auto start = std::chrono::system_clock::now();
		Debug<DebugFlag::PrintPlayerTimer>::get() << "[process - " << turn << "] Before move: " <<
			std::chrono::duration_cast<std::chrono::milliseconds>(player[turn].playerTimer).count() << "ms\n";
		/* Read from child’s stdout. If the time exceeds the player's time left, means he ran out of time. */
		try {
			process[turn].read(word, ChessGame::bufferSize, PipedProcess::ReadFlags::NoSpecialChars
				| PipedProcess::ReadFlags::Timer, player[turn].playerTimer);
		}
		catch (piped_process_exception &e) {
			Debug<DebugFlag::PrintGameEachTurn>::get() << "[process - " << turn << "] " << e.what() << "\n";
			assert(process[turn].writeln(CommunicationProtocol::lossMessage) > 0);
			assert(process[turn^1].writeln(CommunicationProtocol::winMessage) > 0);
			this->logger << "\n" << e.what() <<"\n";
			this->exitGame(0);
		}
		auto end = std::chrono::system_clock::now();
		Debug<DebugFlag::PrintGameEachTurn>::get() << "[process - " << turn << "] " << word << "\n";

		/* Write to the game logger. */
		if(moveNumber % 2 == 0)
			this->logger << word << " ";
		else
			this->logger << word << "\n";

		/* If one of the players asks for a draw, send it to the other player and wait for his response */
		if(word == CommunicationProtocol::drawRequestMessage) {
			if(moveNumber % 2 == 0)
				this->logger << moveNumber / 2 << ". ";

			this->drawRequest(word, turn);

			/* 1. DRAW 2. GO (and we add \n). Otherwise, the \n is added by the previous one. */
			if(moveNumber % 2 == 1)
				this->logger << "\n";
			continue;
		}

		/* Update the player's timer based on how much time he wasted if it wasn't a pause request */
		auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);
		player[turn].playerTimer -= elapsedTime;
		Debug<DebugFlag::PrintPlayerTimer>::get() << "[process - " << turn << "] After move: " <<
			std::chrono::duration_cast<std::chrono::milliseconds>(player[turn].playerTimer).count() << "ms\n";

		/* Check if the player has any time left before proceeding further. */
		if(player[turn].playerTimer.count() <= 0) {
			Debug<DebugFlag::PrintGameEachTurn>::get() << "[process - " << turn << "] Out of time!\n";
			assert(process[turn].writeln(CommunicationProtocol::lossMessage) > 0);
			assert(process[turn^1].writeln(CommunicationProtocol::winMessage) > 0);
			this->logger << "\nOut of time!\n";
			this->exitGame(0);
		}
		player[turn].playerTimer += std::chrono::seconds(CommunicationProtocol::settings["Increment"]);
		Debug<DebugFlag::PrintPlayerTimer>::get() << "[process - " << turn << "] After increment: " <<
			std::chrono::duration_cast<std::chrono::milliseconds>(player[turn].playerTimer).count() << "ms\n";

		/* After we received a move from P1, we send P2 his move and we send him back:
		 * - INVALID, if the move is invalid
		 * - WIN, if the opponent's move is invalid or he won the game
		 * - DRAW, if the game was drawn by repetition or opponent's inability to move
		 * - LOSS, if the opponent's move has mated him
		 * - a move, based on what the opponent moves.
		 */
		auto move = this->parseMove(word);
		if(!std::get<0>(move)) {
			assert(process[turn].writeln(CommunicationProtocol::invalidMoveMessage) > 0);
			assert(process[turn^1].writeln(CommunicationProtocol::winMessage) > 0);
			Debug<DebugFlag::PrintGameEachTurn>::get() << "Ill formed move!\n";
			this->logger << "\nIll formed move!\n";
			this->exitGame(0);
		}
		auto result = this->move(std::get<1>(move), player[turn]);
		if(!result) {
			assert(process[turn].writeln(CommunicationProtocol::invalidMoveMessage) > 0);
			assert(process[turn^1].writeln(CommunicationProtocol::winMessage) > 0);
			Debug<DebugFlag::PrintGameEachTurn>::get() << "Illegal move!\n";
			this->logger << "\nIllegal move!\n";
			this->exitGame(0);
		}

		auto matePosition = this->isMatePosition(player[turn^1]);
		if(matePosition) {
			/* Not a mate now => draw */
			if(!this->isInCheck(player[turn^1])) {
				assert(process[turn].writeln(CommunicationProtocol::drawMessage) > 0);
				assert(process[turn^1].writeln(CommunicationProtocol::drawMessage) > 0);
				Debug<DebugFlag::EndGameStatus>::get() << "Draw!\n";
				this->logger << "\nDraw!\n";
			}
			/* Otherwise, the opponent got mated */
			else {
				assert(process[turn].writeln(CommunicationProtocol::winMessage) > 0);
				assert(process[turn^1].writeln(CommunicationProtocol::lossMessage) > 0);
				Debug<DebugFlag::EndGameStatus>::get() << "Mate by " << player[turn].color.toString() << "\n";
				this->logger << "\nMate by "<< player[turn].color.toString() << "\n";
			}
			this->exitGame(0);
		}

		player[turn].moves.push_back(word);
		/* Check for 4 moves repetition */
		if(this->movesRepetition(player[turn], player[turn^1])) {
			assert(process[turn].writeln(CommunicationProtocol::drawMessage) > 0);
			assert(process[turn^1].writeln(CommunicationProtocol::drawMessage) > 0);
			Debug<DebugFlag::EndGameStatus>::get() << "Draw!\n";
			this->logger << "\nDraw \n";
			this->exitGame(0);
		}

		/* Send the enemy his move */
		assert(process[turn^1].writeln(word) > 0);
		turn ^= 1;
		++this->moveNumber;

		Debug<DebugFlag::PrintGameEachTurn >= 2>::get() << this->toString() << "\n";
	}
}

std::tuple<bool, std::tuple<int, int, int, int, char>> ChessGame::parseMove(const std::string &move) {
	if(move.length() != 4 && move.length() != 5)
		return {false, {0, 0, 0, 0, 0}};
	int oldI = move.c_str()[1] - '1';
	int oldJ = move.c_str()[0] - 'a';
	int newI = move.c_str()[3] - '1';
	int newJ = move.c_str()[2] - 'a';
	char promotion = 0;

	auto x = oldI | oldJ | newI | newJ;
	/* x == 0 only if all are 0, which is also illegal move */
	if(x <= 0 || x >= 8 || (oldI == newI && oldJ == newJ))
		return {false, {0, 0, 0, 0, 0}};

	if(move.length() == 5) {
		promotion = move.c_str()[4];
		if(promotion != 'b' && promotion != 'n' && promotion != 'q' && promotion != 'r')
			return {false, {0, 0, 0, 0, 0}};
	}

	return {true, {oldI, oldJ, newI, newJ, promotion}};
}

bool ChessGame::move(const std::tuple<int, int, int, int, char> &positions, Player &player) {
	int oldI, oldJ, newI, newJ;
	char promotion;
	std::tie(oldI, oldJ, newI, newJ, promotion) = positions;

	/* Remove any stored information about previous last moves */
	this->mainMoveLastMoved = -1;
	this->modifiedPieces.clear();

	unsigned int p1PiecesCount = 0, p2PiecesCount = 0;
	if(player.opponent) {
		p1PiecesCount = player.piecesPosition.size();
		p2PiecesCount = player.opponent->piecesPosition.size();
	}

	if(!this->preMoveChecks({oldI, oldJ, newI, newJ}, player))
		return false;

	if(!this->preMoveActions({oldI, oldJ, newI, newJ}, player))
		return false;

	/* Make the actual move */
	board[newI][newJ] = std::move(board[oldI][oldJ]);
	board[oldI][oldJ] = NoPiece::get();

	if(!this->postMoveActions(positions, player))
		return false;

	/* If any pieces was taken, clear the history map. */
	if(p1PiecesCount != 0 && p2PiecesCount != 0 && (player.piecesPosition.size() != p1PiecesCount || 
		player.opponent->piecesPosition.size() != p2PiecesCount )) {
		Debug<DebugFlag::PrintHistoryMapFlag>::get() <<"Deleting history map! "<< p1PiecesCount << " " << p2PiecesCount 
			<< " " <<  player.piecesPosition.size()<< " " << player.opponent->piecesPosition.size() << "\n";
		historyMap.clear();
	}

	return true;
}

bool ChessGame::preMoveChecks(const std::tuple<int, int, int, int> & positions, 
	const Player &player) const {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;
	auto piece = board[oldI][oldJ];

	/* Common checks for all pieces: If the color of the piece is not his (this includes NoPiece as well) or if
	 * the position the piece is moving to is a friendly unit then the move is invalid */
	if(piece->getColor() != player.color || board[newI][newJ]->getColor() == player.color)
		return false;

	/* You cannot take a king */
	if(board[newI][newJ]->getType() == "King")
		return false;

	if(!piece->isValidMove(this->board, this->moveNumber, {oldI, oldJ, newI, newJ}))
		return false;

	if(piece->getType() == "King" && abs(oldJ-newJ) > 1 && !this->canCastle({oldI, oldJ, newI, newJ}, player))
		return false;

	return true;
}

bool ChessGame::preMoveActions(const std::tuple<int, int, int, int> &positions, 
	Player &player) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;
	auto piece = board[oldI][oldJ];

	/* En passant check is before we change old pieces */
	if(piece->getType() == "Pawn" && board[newI][newJ]->getType() == "NoPiece" && abs(newJ-oldJ) == 1) {
		this->modifiedPieces.push_back(std::make_tuple(oldI, newJ, std::shared_ptr<Piece>(board[oldI][newJ])));
		board[oldI][newJ] = std::make_shared<NoPiece>();
		if(player.opponent)
			player.opponent->removePiece(std::make_tuple(oldI, newJ), board[oldI][oldJ]->getType());
	}

	/* Remove the enemy piece if there is one */
	if(Color::getOppositeColor(board[newI][newJ]->getColor()) == player.color) {
		if(player.opponent)
			player.opponent->removePiece(std::make_tuple(newI, newJ), board[oldI][oldJ]->getType());
	}

	this->modifiedPieces.push_back(std::make_tuple(newI, newJ, std::shared_ptr<Piece>(board[newI][newJ])));
	this->modifiedPieces.push_back(std::make_tuple(oldI, oldJ, std::shared_ptr<Piece>(board[oldI][oldJ])));
	player.removePiece(std::make_tuple(oldI, oldJ), board[oldI][oldJ]->getType());

	return true;
}

bool ChessGame::postMoveActions(const std::tuple<int, int, int, int, char> &positions,
	Player &player) {
	int oldI, oldJ, newI, newJ;
	char promotion;
	std::tie(oldI, oldJ, newI, newJ, promotion) = positions;

	/* Pawn promotion */
	if(promotion != 0) {
		if(board[newI][newJ]->getType() != "Pawn")
			return false;
		if( (player.color == Color::White && newI != 7) || (player.color == Color::Black && newI != 0) )
			return false;

		if(promotion == 'b')
			board[newI][newJ] = std::make_shared<Bishop>(player.color);
		else if(promotion == 'n')
			board[newI][newJ] = std::make_shared<Knight>(player.color);
		else if(promotion == 'q')
			board[newI][newJ] = std::make_shared<Queen>(player.color);
		else if(promotion == 'r')
			board[newI][newJ] = std::make_shared<Rook>(player.color);
		else
			return false;

		return true;
	}

	/* Forced pawn promotion */
	if(promotion == 0 && board[newI][newJ]->getType() == "Pawn" && (newI == 0 || newI == 7)) {
		board[newI][newJ] = std::make_shared<Queen>(player.color);
		return true;
	}

	/* Update king position for this player and disable castling. */
	if(board[newI][newJ]->getType() == "King") {
		player.kingPosition = {newI, newJ};
		/* Disable castling for future */
		if(auto king = dynamic_cast<King *>(board[newI][newJ].get())) {
			king->lastMoveCouldCastle = king->canCastle;
			king->canCastle = false;
		}
		else
			throw 1;

		/* Castle to right, move rook */
		if(newJ - oldJ == 2) {
			this->modifiedPieces.push_back(std::make_tuple(oldI, newJ+1, std::shared_ptr<Piece>(board[oldI][newJ+1])));
			this->modifiedPieces.push_back(std::make_tuple(oldI, newJ-1, std::shared_ptr<Piece>(board[oldI][newJ-1])));
			std::swap(board[oldI][newJ+1], board[oldI][newJ-1]);
			player.removePiece(std::make_tuple(oldI, newJ+1), board[oldI][newJ+1]->getType());
			player.addPiece(std::make_tuple(oldI, newJ-1), board[oldI][newJ-1]->getType());
		}
		/* Castle to left, move rook */
		else if(newJ - oldJ == -2) {
			this->modifiedPieces.push_back(std::make_tuple(oldI, newJ-2, std::shared_ptr<Piece>(board[oldI][newJ-2])));
			this->modifiedPieces.push_back(std::make_tuple(oldI, newJ+1, std::shared_ptr<Piece>(board[oldI][newJ+1])));
			std::swap(board[oldI][newJ-2], board[oldI][newJ+1]);
			player.removePiece(std::make_tuple(oldI, newJ-2), board[oldI][newJ-2]->getType());
			player.addPiece(std::make_tuple(oldI, newJ+1), board[oldI][newJ+1]->getType());
		}
	}

	/* Disable castling for this rook. */
	if(board[newI][newJ]->getType() == "Rook") {
		/* Disable castle for this rook */
		if(auto rook = dynamic_cast<Rook *>(board[newI][newJ].get())){
			rook->lastMoveCouldCastle = rook->canCastle;
			rook->canCastle = false;
		}
		else
			throw 1;
	}

	if(this->isInCheck(player))
		return false;

	/* Save the previous last moved state for undo move (if needed) */
	this->mainMoveLastMoved = board[newI][newJ]->lastMoved; 
	board[newI][newJ]->lastMoved = this->moveNumber;
	player.addPiece(std::make_tuple(newI, newJ), board[newI][newJ]->getType());
	return true;
}

bool ChessGame::canCastle(const std::tuple<int, int, int, int> &positions, 
	const Player &player) const {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;

	if(abs(newJ - oldJ) != 2 || oldI != newI)
		return false;

	auto _kingPiece = board[oldI][oldJ];
	auto _rookPiece = (newJ - oldJ == 2) ? board[oldI][newJ+1] : board[oldI][newJ-2];
	King *king = dynamic_cast<King *>(_kingPiece.get());
	Rook *rook = dynamic_cast<Rook *>(_rookPiece.get());

	if(board[oldI][oldJ]->getType() != "King" || _rookPiece->getType() != "Rook" || !king || !rook)
		return false;

	if(!king->canCastle || !rook->canCastle)
		return false;

	if(this->isPieceBetween(oldI, oldJ, newI, newJ))
		return false;

	/* Now I must check if there's any check on the way */
	/* The only 3 ways this can be a problem is if i'm right now in a check or the square in between the move is in 
	 * check. The final isInCheck will verify if the end position is in check and that'll be illegal too. */
	if(this->isInCheck(Player(player.color, {oldI, oldJ})) 
		|| this->isInCheck(Player(player.color, {oldI, (oldJ+newJ)/2})))
		return false;

	return true;
}

bool ChessGame::isInCheck(const Player &player) const {
	int kingI, kingJ;

	/* We must be sure that the parameter we send has a good king position (canCastle will abuse this to check if the
	 * king can castle, and use the in-between positions and check if there's a check there */
	std::tie(kingI, kingJ) = player.kingPosition;

	/* Check if any opponent's pieces might attack the king. */
	/* Knight */
	auto knightPositions = Knight::knightTable.values;
	for(int i=0; knightPositions[KnightTable::index(kingI, kingJ, i)].first() != -1; ++i) {
		auto positions = knightPositions[KnightTable::index(kingI, kingJ, i)];
		auto piece = board[positions.first()][positions.second()];
		if(isInCheckType(player, piece, "Knight"))
			return true;
	}

	/* Bishop & Queen */
	auto bishopPositions = Bishop::bishopTable.values;
	for(int i=0; bishopPositions[BishopTable::index(kingI, kingJ, i)].first() != -1; ++i) {
		auto positions = bishopPositions[BishopTable::index(kingI, kingJ, i)];
		auto piece = board[positions.first()][positions.second()];
		if( (isInCheckType(player, piece, "Bishop") || isInCheckType(player, piece, "Queen"))
			&& !isPieceBetween(kingI, kingJ, positions.first(), positions.second()))
			return true;
	}

	/* Rook & Queen */
	auto rookPositions = Rook::rookTable.values;
	for(int i=0; rookPositions[RookTable::index(kingI, kingJ, i)].first() != -1; ++i) {
		auto positions = rookPositions[RookTable::index(kingI, kingJ, i)];
		auto piece = board[positions.first()][positions.second()];
		if( (isInCheckType(player, piece, "Rook") || isInCheckType(player, piece, "Queen"))
			&& !isPieceBetween(kingI, kingJ, positions.first(), positions.second()))
			return true;
	}

	/* Pawns */
	auto pawnPositions = (player.color == Color::White ? Pawn::whitePawnCheckTable.values :
		Pawn::blackPawnCheckTable.values);
	for(int i=0; pawnPositions[PawnCheckTable::index(kingI, kingJ, i)].first() != -1; ++i) {
		auto positions = pawnPositions[PawnCheckTable::index(kingI, kingJ, i)];
		auto piece = board[positions.first()][positions.second()];
		if(isInCheckType(player, piece, "Pawn"))
			return true;
	}

	/* Kings */
	auto kingPositions = King::kingTable.values;
	for(int i=0; kingPositions[KingTable::index(kingI, kingJ, i)].first() != -1; ++i) {
		auto positions = kingPositions[KingTable::index(kingI, kingJ, i)];
		auto piece = board[positions.first()][positions.second()];
		if(isInCheckType(player, piece, "King"))
			return true;
	}

	return false;
}

bool ChessGame::isPieceBetween(int firstI, int firstJ, int secondI, int secondJ) const {
	/* From left to right */
	if(firstI == secondI) {
		if(firstJ > secondJ)
			std::swap(firstJ, secondJ);
		for(int j=firstJ+1; j<secondJ; ++j)
			if(board[firstI][j]->getColor() != Color::None)
				return true;
	}
	/* We're checking from up to down */
	else if(firstJ == secondJ) {
		if(firstI > secondI)
			std::swap(firstI, secondI);
		for(int i=firstI+1; i<secondI; ++i)
			if(board[i][firstJ]->getColor() != Color::None)
				return true;
	}
	/* Diagonally */
	else {
		/* If correctly diagonal, the difference is the same. Example: for (5,3) you get [(6,4) (7,5) (4,2) (3,1)
		 * (2,0) (6,2) (7,1) (4,4), (3,5) (2,6), (1,7)]. All the differences are the same in abs. */
		if(abs(firstI-secondI) != abs(firstJ-secondJ))
			return false;

		int signI = (firstI > secondI) ? -1 : 1;
		int signJ = (firstJ > secondJ) ? -1 : 1;

		for(int i=firstI+signI, j=firstJ+signJ; i!=secondI; i+=signI, j+=signJ)
			if(board[i][j]->getColor() != Color::None)
				return true;
	}

	return false;
}

bool ChessGame::isInCheckType(const Player &player, const std::shared_ptr<Piece> &piece,
	const std::string &type) const {
	return piece->getType() == type && Color::getOppositeColor(piece->getColor()) == player.color;
}

bool ChessGame::isMatePosition(Player &player) {
	int pieceI, pieceJ;
	const Pair<int, int> *positions;

	/* For efficiency, check if I can move the king first (most of the checks are like this) */
	std::tie(pieceI, pieceJ) = player.kingPosition;
	positions = King::kingTable.values;
	auto kingIndex = KingTable::index;
	for(auto i = 0; positions[kingIndex(pieceI, pieceJ, i)].first() != -1; ++i) {
		int newI = positions[kingIndex(pieceI, pieceJ, i)].first();
		int newJ = positions[kingIndex(pieceI, pieceJ, i)].second();
		Player fakePlayer = Player(player.color, player.kingPosition);

		++this->moveNumber;
		auto rc = this->move({pieceI, pieceJ, newI, newJ, 0}, fakePlayer);
		--this->moveNumber;
		/* Undo it for correctness (even if no move was actually done in the modifiedPieces array, because 
		 * a catastrophical bad move was tried, like moving over your own piece */
		this->undoMove({pieceI, pieceJ, newI, newJ});
		/* If the move is legal then the position is not a mate position */
		if(rc)
			return false;
	}

	/* Use the piecesPosition set here */
	for(auto &item : player.piecesPosition) {
		std::tie(pieceI, pieceJ) = item;
		auto piece = board[pieceI][pieceJ];
		auto type = piece->getType();

		if(type == "King")
			continue;

		positions = (type == "Pawn" && player.color == Color::White) ? Pawn::whitePawnMoveTable.values
			: (type == "Pawn" && player.color == Color::Black) ? Pawn::blackPawnMoveTable.values
			: (type == "Bishop" || type == "Queen") ? Bishop::bishopTable.values
			: (type == "Knight") ? Knight::knightTable.values
			: Rook::rookTable.values;

		auto index = (type == "Pawn") ? PawnMoveTable::index
			: (type == "Bishop" || type == "Queen") ? BishopTable::index
			: (type == "Knight") ? KnightTable::index
			: RookTable::index;

		for(auto i = 0; positions[index(pieceI, pieceJ, i)].first() != -1; ++i) {
			int newI = positions[index(pieceI, pieceJ, i)].first();
			int newJ = positions[index(pieceI, pieceJ, i)].second();
			Player fakePlayer = Player(player.color, player.kingPosition);
			/* Do the potential move */
			++this->moveNumber;
			auto rc = this->move({pieceI, pieceJ, newI, newJ, 0}, fakePlayer);
			--this->moveNumber;
			/* Undo it for correctness (even if no move was actually done in the modifiedPieces array, because 
			 * a catastrophical bad move was tried, like moving over your own piece */
			this->undoMove({pieceI, pieceJ, newI, newJ});
			/* If the move is legal then the position is not a mate position */
			if(rc)
				return false;
		}

		/* Queens are also rooks so redo the process. */
		if(type == "Queen") {
			positions = Rook::rookTable.values;
			index = RookTable::index;

			for(auto i = 0; positions[index(pieceI, pieceJ, i)].first() != -1; ++i) {
				int newI = positions[index(pieceI, pieceJ, i)].first();
				int newJ = positions[index(pieceI, pieceJ, i)].second();
				Player fakePlayer = Player(player.color, player.kingPosition);
			
				++this->moveNumber;
				auto rc = this->move({pieceI, pieceJ, newI, newJ, 0}, fakePlayer);
				--this->moveNumber;

				this->undoMove({pieceI, pieceJ, newI, newJ});
				/* If the move is legal then the position is not a mate position */
				if(rc)
					return false;
			}
		}
	}

	return true;
}

void ChessGame::undoMove(const std::tuple<int, int, int, int> &positions) {
	int oldI, oldJ, newI, newJ;

	std::tie(oldI, oldJ, newI, newJ) = positions;
	Debug<DebugFlag::PrintUndo>::get() << "Undoing: oldI=" << oldI << " oldJ=" << oldJ <<" newI=" << newI << " newJ="
		<< newJ << " | Main move count=" << this->mainMoveLastMoved << "\n";

	if(this->modifiedPieces.empty())
		return;

	/* Loop through all saved modified pieces, put them back on the table and add them back to the player pieces set */
	for(auto &t : this->modifiedPieces) {
		int i, j;
		std::shared_ptr<Piece> p;
		std::tie(i, j, p) = t;

		Debug<DebugFlag::PrintUndo>::get() << "i=" << i << " j=" << j << " | Piece type="
			<< p->getType() <<" | Piece color="<<p->getColor().toString() << "\n";

		board[i][j] = p;
	}

	board[oldI][oldJ]->lastMoved = this->mainMoveLastMoved;

	/* If the king was move and he could castle before, make him castle again */
	if(board[oldI][oldJ]->getType() == "King") {
		if(auto king = dynamic_cast<King *>(board[oldI][oldJ].get()))
			king->canCastle = king->lastMoveCouldCastle;
		else {
			throw 1;
		}
	}

	/* Same for Rook. Note, that if we castled, we don't need to set the rook to uncastlable because only one castle
	 * can be done. So as long as the rook wasn't moved before, we can castle. If we castled, whatever happens next
	 * is irelevant. That's why here we don't need to make the rook castlable again if it was a castle.  */
	if(board[oldI][oldJ]->getType() == "Rook") {
		if(auto rook = dynamic_cast<Rook *>(board[oldI][oldJ].get()))
			rook->canCastle = rook->lastMoveCouldCastle;
		else {
			throw 1;
		}
	}
}

void ChessGame::Player::removePiece(const std::tuple<int, int> &positions, const std::string &type) {
	/* Very important check because this doesn't do anything on FakePlayers. */
	if(piecesPosition.empty())
		return;

	auto first = std::get<0>(positions);
	auto second = std::get<1>(positions);

	auto it = piecesPosition.find(positions);
	if(it != piecesPosition.end())
		piecesPosition.erase(it);
	piecesBitMap[piecesBitMapOrder[type]].set(first * 8 + second, 0);
}

void ChessGame::Player::addPiece(const std::tuple<int, int> &positions, const std::string &type) {
	if(piecesPosition.empty())
		return;

	auto first = std::get<0>(positions);
	auto second = std::get<1>(positions);

	piecesPosition.emplace(positions);
	piecesBitMap[piecesBitMapOrder[type]].set(first * 8 + second);
}

bool ChessGame::movesRepetition(const Player &p1, const Player &p2) {
	std::array<std::bitset<64>, 12> key = {p1.piecesBitMap[0], p1.piecesBitMap[1], p1.piecesBitMap[2],
		p1.piecesBitMap[3], p1.piecesBitMap[4], p1.piecesBitMap[5], p2.piecesBitMap[0], p2.piecesBitMap[1],
		p2.piecesBitMap[2], p2.piecesBitMap[3], p2.piecesBitMap[4], p2.piecesBitMap[5] };

	if(DebugFlag::PrintHistoryMapFlag) {
		std::string historyStr = "Key=";
		for(int i=0; i<12; i++)
			historyStr.append(std::to_string(key[i].to_ullong()) + " ");
		historyStr.append("\nHistory Map:\n");
		for(auto &x : historyMap) {
			for(auto i = 0 ; i < 12; i++)
				historyStr.append(std::to_string(x.first[i].to_ullong()) + " ");
			historyStr.append(x.second + "\n");
		}
		Debug<DebugFlag::PrintHistoryMapFlag>::get() << historyStr << "\n";
	}

	try {
		auto x = ++historyMap.at(key);
		Debug<DebugFlag::PrintHistoryMapFlag>::get() << "Key found, value=" << x << "\n";
		if(x == CommunicationProtocol::settings["RepetitionDraw"])
			return true;
	}
	catch (const std::out_of_range &oor) {
		Debug<DebugFlag::PrintHistoryMapFlag>::get() << "Key not found, adding it\n";
		historyMap.emplace(key, 1);
	}

	return false;
}

void ChessGame::exitGame(int exit_status) {
	Debug<DebugFlag::PrintGameEachTurn >= 2>::get() << this->toString() << "\n";
	this->logger << "\n" << this->toString() << "\n";
	exit(exit_status);
}

ChessGame::~ChessGame() { }
