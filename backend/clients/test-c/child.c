#include "child.h"

static char buffer[BUFSIZE];
static int rc;
static int color;

void processSettings(char *buffer) {
	/* create initial state for the game */
	color = buffer[6] - '0';
}

void initialChessMessages() {
	/* Send the initialization message to start a chess game */
	flPrintf(INIT_MESSAGE"\n");

	/* Now I am reading the settings of the game and I must parse them accordingly. */
	whRead(rc, STDIN_FILENO, buffer, BUFSIZE);
	processSettings(buffer);

	/* After having processed the settings, I am ready to start the game. */
	flPrintf(READY_MESSAGE);
}

void initialMessages() {
	/* 3 way handshake to initialize the new process */
	readCompare(rc, buffer, BUFSIZE, HANDSHAKE_INITIAL_MESSAGE);
	flPrintf(HANDSHAKE_ACCEPT_MESSAGE"\n");
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);
}

void processMove(char *move) {
	/* Process move */
}

int main(int argc, char **argv) {
	initialMessages();
	initialChessMessages();

	/* if color == White => move first and read go message. Black will read white's move as a go message. */
	if(color == 0) {
		readCompare(rc, buffer, BUFSIZE, GO_MESSAGE);
		flPrintf("a2a4");
	}

	while(1) {
		memset(buffer, 0, BUFSIZE);
		whRead(rc, STDIN_FILENO, buffer, BUFSIZE);

		processMove(buffer);
		flPrintf("a7a5\n"); /* Here child sends chess moves (eg: a7a5) */
		
		sleep(5);
	}

	return 0;
}