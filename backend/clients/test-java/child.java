import java.util.Scanner;
import java.lang.*;

/* For java: javac child.java && jar cvfe Child.jar Child Child.class
 * Run: /usr/bin/java -jar /path/to/Child.jar Child
 */

class Child {
	public static final String handshakeInitialMessage = "MAIN";
	public static final String handshakeAcceptMessage = "ACCEPT";
	public static final String ackMessage = "OK";
	public static final String initMessage = "INIT";
	public static final String readyMessage = "READY";
	public static final String goMessage = "GO";
	public static final String invalidMoveMessage = "INVALID";
	public static final String winMessage = "WIN";


	public static Scanner scan = new Scanner(System.in);
	public static String myLine;

	public static int color = 0;

	public static void processSettings(String line) {
		/* Create initial state for the chess game */
		color = line.charAt(6) - '0';
	}

	public static void initialChessMessages() throws Exception {
		/* Send the initialization message to start a chess game */
		System.out.println(initMessage);

		/* Now I am reading the settings of the game and I must parse them accordingly. */
		myLine = scan.nextLine();
		processSettings(myLine);

		/* After having processed the settings, I am ready to start the game. */
		System.out.println(readyMessage);
	}

	public static void initialMessages() throws Exception {
		myLine = scan.nextLine();
		if(!myLine.equals(handshakeInitialMessage))
			throw new Exception("Wrong first message");
	
		System.out.println(handshakeAcceptMessage);

		myLine = scan.nextLine();
		if(!myLine.equals(ackMessage))
			throw new Exception("Not an ACK message");
	}

	public static void processMove(String move) {
		// process the move
		System.out.println("a7a5");
	}

	public static void main(String[] args) throws Exception {
		//System.err.println("[JAVA] Starting Child");
		initialMessages();
		initialChessMessages();

		if(color == 0) {
			myLine = scan.nextLine();
			if(!myLine.equals(goMessage))
				throw new Exception("Not the start message.");
			System.out.println("a2a4");
		}

		Thread.sleep(2500);
		while(true) {
			myLine = scan.nextLine();
			if(myLine.equals(winMessage))
				throw new Exception("Win");
			else if(myLine.equals(invalidMoveMessage))
				throw new Exception("Invalid move");

			processMove(myLine);
			Thread.sleep(5000);
		}
	}
}